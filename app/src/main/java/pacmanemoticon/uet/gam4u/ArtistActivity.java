package pacmanemoticon.uet.gam4u;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;

import java.lang.reflect.Array;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

import pacmanemoticon.uet.gam4u.adapter.ListArtistAdapter;
import pacmanemoticon.uet.gam4u.adapter.ListSongAdapter;
import pacmanemoticon.uet.gam4u.database.MyDatabase;
import pacmanemoticon.uet.gam4u.dialog.SearchDialog;
import pacmanemoticon.uet.gam4u.model.Artist;
import pacmanemoticon.uet.gam4u.model.Song;


/**
 * Created by anhdt on 1/1/2016.
 */
public class ArtistActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {
    private DrawerLayout drawer;
    MyDatabase database;
    private ListView listArtist;

    ArrayList<Artist> artistArrayList;
    ListArtistAdapter listArtistAdapter;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_artist);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close) {
            @Override
            public void onDrawerClosed(View drawerView) {
                super.onDrawerClosed(drawerView);
            }

            @Override
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
            }

            @Override
            public void onDrawerSlide(View drawerView, float slideOffset) {
                super.onDrawerSlide(drawerView, slideOffset);
            }

            @Override
            public void onDrawerStateChanged(int newState) {
                super.onDrawerStateChanged(newState);
            }
        };
        toggle.setDrawerIndicatorEnabled(true);
        toggle.syncState();
        drawer.setDrawerListener(toggle);
        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        getSupportActionBar().setTitle("Danh sách ca sĩ");

        listArtist = (ListView) findViewById(R.id.listArtist);
        try {
            database = new MyDatabase(this).open();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        database.createSampleData();
        artistArrayList = database.getListArtist();
        Collections.sort(artistArrayList, new Comparator<Artist>(){
            @Override
            public int compare(Artist a1, Artist a2){
                return (a1.getArtistName().compareTo(a2.getArtistName()));
            }
        });
        listArtistAdapter = new ListArtistAdapter(this, artistArrayList);
        listArtist.setAdapter(listArtistAdapter);
        setListViewHeightBasedOnChildren(listArtist);
        listArtist.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Artist artist = artistArrayList.get(position);
                Intent intent = new Intent(ArtistActivity.this,SongActivity.class);
                intent.putExtra("singer_name", artist.getArtistName());
                intent.putExtra("mode", 2);
                startActivity(intent);
            }
        });
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }


    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        Intent intent;
        switch (item.getItemId()){
            case R.id.navHomePage:
                intent = new Intent(ArtistActivity.this,MainActivity.class);
                startActivity(intent);
                break;
            case R.id.navSongLibrary:
                intent = new Intent(ArtistActivity.this,SongActivity.class);
                intent.putExtra("mode", 0);
                startActivity(intent);
                break;
            case R.id.navArtist:
                break;
            case R.id.navGam:
                intent = new Intent(ArtistActivity.this,ChordMenuActivity.class);
                startActivity(intent);
                break;
            case R.id.navFavorite:
                intent = new Intent(ArtistActivity.this,SongActivity.class);
                intent.putExtra("mode",1);
                startActivity(intent);
                break;
            case R.id.navSetting:{}
            case R.id.navLogin:
                intent = new Intent(this, LoginActivity.class);
                startActivity(intent);
                break;
        }
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    public void setListViewHeightBasedOnChildren(ListView listView) {
        ListAdapter listAdapter = listView.getAdapter();
        if (listAdapter == null) {
            // pre-condition
            return;
        }

        int totalHeight = 0;
        for (int i = 0; i < listAdapter.getCount(); i++) {
            View listItem = listAdapter.getView(i, null, listView);
            listItem.measure(0, 0);
            totalHeight += listItem.getMeasuredHeight();
        }

        ViewGroup.LayoutParams params = listView.getLayoutParams();
        params.height = totalHeight + (listView.getDividerHeight() * (listAdapter.getCount() - 1));
        listView.setLayoutParams(params);
    }
}
