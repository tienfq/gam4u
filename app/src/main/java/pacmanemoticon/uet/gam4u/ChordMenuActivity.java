package pacmanemoticon.uet.gam4u;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.view.View.OnClickListener;

import com.dqt.libs.chorddroid.components.ChordTextureView;

import java.util.ArrayList;
import java.util.List;

import pacmanemoticon.uet.gam4u.model.Chord;

/**
 * Created by anhdt on 1/12/2015.
 */

public class ChordMenuActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener{
    ImageView imageView;
    ChordTextureView chordView;
    TextView txtHandPosition;
    int position = 0; // fret position index (0 to 12)
    private Spinner spinner1, spinner2;
    private DrawerLayout drawer;

    Button submit;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chord);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close) {
            @Override
            public void onDrawerClosed(View drawerView) {
                super.onDrawerClosed(drawerView);
            }

            @Override
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
            }

            @Override
            public void onDrawerSlide(View drawerView, float slideOffset) {
                super.onDrawerSlide(drawerView, slideOffset);
            }

            @Override
            public void onDrawerStateChanged(int newState) {
                super.onDrawerStateChanged(newState);
            }
        };
        toggle.setDrawerIndicatorEnabled(true);
        toggle.syncState();
        drawer.setDrawerListener(toggle);
        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        getSupportActionBar().setTitle("Tra cứu hợp âm");
        addItemsOnSpinner1();
        addItemsOnSpinner2();
        // Find views
        chordView = (ChordTextureView) findViewById(R.id.chord_texture_view);
        txtHandPosition = (TextView) findViewById(R.id.txt_hand_position);
        // First draw
        spinner1 = (Spinner) findViewById(R.id.spinner_chord);
        spinner2 = (Spinner) findViewById(R.id.spinner_rela);
        submit = (Button)findViewById(R.id.submit);

        chordView.drawChord("C", 0);
        submit.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                if(spinner2.getSelectedItem().equals("major")){
                    position = 0;
                    chordView.drawChord(String.valueOf(spinner1.getSelectedItem()), position);
                }
                else{
                    position = 0;
                    chordView.drawChord(String.valueOf(spinner1.getSelectedItem()) + String.valueOf(spinner2.getSelectedItem()), position);
                }
            }

        });

        // Events
        findViewById(R.id.btn_increase).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (position < 8) {
                    position++;
                    if(spinner2.getSelectedItem().equals("major")){
                        chordView.drawChord(String.valueOf(spinner1.getSelectedItem()), position);
                    }
                    else{
                        chordView.drawChord(String.valueOf(spinner1.getSelectedItem()) + String.valueOf(spinner2.getSelectedItem()), position);
                    }
                }
            }
        });
        findViewById(R.id.btn_decrease).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (position > 0) {
                    position--;
                    if(spinner2.getSelectedItem().equals("major")){
                        chordView.drawChord(String.valueOf(spinner1.getSelectedItem()), position);
                    }
                    else{
                        chordView.drawChord(String.valueOf(spinner1.getSelectedItem()) + String.valueOf(spinner2.getSelectedItem()), position);
                    }
                }
            }
        });
    }

    public void addItemsOnSpinner1() {

        spinner1 = (Spinner) findViewById(R.id.spinner_chord);
        List<String> list = new ArrayList<String>();
        list.add("C");
        list.add("D");
        list.add("E");
        list.add("F");
        list.add("G");
        list.add("A");
        list.add("B");
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_spinner_item, list);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner1.setAdapter(dataAdapter);
    }

    public void addItemsOnSpinner2() {

        spinner2 = (Spinner) findViewById(R.id.spinner_rela);
        List<String> list = new ArrayList<String>();
        list.add("major");
        list.add("m");
        list.add("sus7");
        list.add("sus");
        list.add("dim");
        list.add("#m");
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_spinner_item, list);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner2.setAdapter(dataAdapter);
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        Intent intent;
        switch (item.getItemId()) {
            case R.id.navHomePage:
                intent = new Intent(ChordMenuActivity.this,MainActivity.class);
                startActivity(intent);
                break;
            case R.id.navSongLibrary:
                intent = new Intent(ChordMenuActivity.this, SongActivity.class);
                intent.putExtra("mode", 0);
                startActivity(intent);
                break;
            case R.id.navArtist:
                intent = new Intent(this, ArtistActivity.class);
                startActivity(intent);
                break;
            case R.id.navGam:
                break;
            case R.id.navFavorite:
                intent = new Intent(ChordMenuActivity.this, SongActivity.class);
                intent.putExtra("mode", 1);
                startActivity(intent);
                break;
            case R.id.navSetting: {
            }
            case R.id.navLogin:
                intent = new Intent(this, LoginActivity.class);
                startActivity(intent);
                break;
        }
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
}

