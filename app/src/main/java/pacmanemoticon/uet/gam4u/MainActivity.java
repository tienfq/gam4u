package pacmanemoticon.uet.gam4u;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;

import pacmanemoticon.uet.gam4u.model.Song;

/**
 * Created by MaXo on 11/30/2015.
 */
public class MainActivity extends Activity implements View.OnClickListener{
    private ImageButton btnListSongs;
    private ImageButton btnChordMenu;
    private ImageButton btnFavoriteSongs;
    private ImageButton btnSetting;
    public static Song chosenSong;
    private EditText etSearch;
    @Override
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        etSearch = (EditText) findViewById(R.id.etSearch);
        etSearch.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                boolean handled = false;
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    String keyWord = etSearch.getText().toString();
                    if (keyWord.matches("")) return handled;
                    Intent intent = new Intent(MainActivity.this, SearchActivity.class);
                    intent.putExtra("keyword", keyWord);
                    etSearch.setText("");
                    startActivity(intent);
                    handled = true;
                }
                return handled;
            }
        });
        btnListSongs = (ImageButton) findViewById(R.id.btnListSong);
        btnChordMenu = (ImageButton) findViewById(R.id.btnChord);
        btnFavoriteSongs = (ImageButton) findViewById(R.id.btnFavoriteSong);
        btnSetting = (ImageButton) findViewById(R.id.btnSetting);
        btnListSongs.setOnClickListener(this);
        btnChordMenu.setOnClickListener(this);
        btnFavoriteSongs.setOnClickListener(this);
        btnSetting.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        Intent intent;
        switch (v.getId()){
            case R.id.btnListSong:
                intent = new Intent(MainActivity.this,SongActivity.class);
                intent.putExtra("title","Danh sách bài hát");
                intent.putExtra("mode",0);
                startActivity(intent);
                break;
            case R.id.btnChord:
                intent = new Intent(MainActivity.this,ChordMenuActivity.class);
                intent.putExtra("title","Tra cứu hợp âm");
                startActivity(intent);
                break;
            case R.id.btnFavoriteSong:
                intent = new Intent(MainActivity.this,SongActivity.class);
                intent.putExtra("title","Bài hát ưa thích");
                intent.putExtra("mode",1);
                startActivity(intent);
                break;
            default:
                break;
        }
    }
}
