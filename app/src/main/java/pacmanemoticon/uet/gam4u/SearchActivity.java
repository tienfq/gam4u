package pacmanemoticon.uet.gam4u;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import java.sql.SQLException;
import java.util.ArrayList;

import pacmanemoticon.uet.gam4u.adapter.ListSongAdapter;
import pacmanemoticon.uet.gam4u.database.MyDatabase;
import pacmanemoticon.uet.gam4u.model.Song;

/**
 * Created by Nam on 11/30/2015.
 */
public class SearchActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener{
    private DrawerLayout drawer;
    private ListView listSong;
    private ListSongAdapter listSongAdapter;
    private ArrayList<Song> songResultArrayList;
    TextView tvResult;
    private int mode;
    MyDatabase database;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
//        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
//        fab.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
//                        .setAction("Action", null).show();
//            }
//        });
        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close) {
            @Override
            public void onDrawerClosed(View drawerView) {
                super.onDrawerClosed(drawerView);
            }

            @Override
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
            }

            @Override
            public void onDrawerSlide(View drawerView, float slideOffset) {
                super.onDrawerSlide(drawerView, slideOffset);
            }

            @Override
            public void onDrawerStateChanged(int newState) {
                super.onDrawerStateChanged(newState);
            }
        };
        toggle.setDrawerIndicatorEnabled(true);
        toggle.syncState();
        drawer.setDrawerListener(toggle);
        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        Bundle bundle = getIntent().getExtras();
        String keyWord = bundle.getString("keyword");
        getSupportActionBar().setTitle("Kết quả tìm kiếm cho: " + keyWord);
        try {
            database = new MyDatabase(this).open();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        database.createSampleData();
        listSong = (ListView) findViewById(R.id.listSong);
        tvResult = (TextView) findViewById(R.id.tvResult);

        songResultArrayList = database.getSongByName(keyWord);
        if (songResultArrayList.size() >= 1) tvResult.setVisibility(View.GONE);
        else tvResult.setVisibility(View.VISIBLE);
        listSongAdapter = new ListSongAdapter(this, songResultArrayList);
//        favoriteSongAdapter = new ListSongAdapter(this, songFavoriteList);
        listSong.setAdapter(listSongAdapter);
        listSong.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Song song = songResultArrayList.get(position);
                Intent intent = new Intent(SearchActivity.this,SongContentActivity.class);
                intent.putExtra("title",song.getTitle());
                intent.putExtra("content",song.getContent());
                startActivity(intent);
            }
        });
//        listSong2.setAdapter(favoriteSongAdapter);
    }

//    private void createViewByMode(int mode){
//        if (mode == 0){
//            listSong1.setVisibility(View.VISIBLE);
//            listSong2.setVisibility(View.GONE);
//            getSupportActionBar().setTitle("Danh sách bài hát");
//        }
//        else {
//            listSong1.setVisibility(View.GONE);
//            listSong2.setVisibility(View.VISIBLE);
//            getSupportActionBar().setTitle("Bài hát yêu thích");
//        }
//    }
    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        Intent intent;
        switch (item.getItemId()){
            case R.id.navHomePage:
                intent = new Intent(SearchActivity.this,MainActivity.class);
                startActivity(intent);
                break;
            case R.id.navSongLibrary:
                intent = new Intent(SearchActivity.this,SongActivity.class);
                intent.putExtra("mode", 0);
                startActivity(intent);
                break;
            case R.id.navArtist:
                intent = new Intent(SearchActivity.this, ArtistActivity.class);
                startActivity(intent);
                break;
            case R.id.navGam:
                intent = new Intent(SearchActivity.this,ChordMenuActivity.class);
                startActivity(intent);
                break;
            case R.id.navFavorite:
                intent = new Intent(SearchActivity.this,SongActivity.class);
                intent.putExtra("mode",1);
                startActivity(intent);
                break;
            case R.id.navSetting:{}
            case R.id.navLogin:
                intent = new Intent(this, LoginActivity.class);
                startActivity(intent);
                break;
        }
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
}
