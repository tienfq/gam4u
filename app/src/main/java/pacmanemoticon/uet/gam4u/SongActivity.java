package pacmanemoticon.uet.gam4u;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;

import java.sql.SQLException;
import java.util.ArrayList;

import pacmanemoticon.uet.gam4u.adapter.ListSongAdapter;
import pacmanemoticon.uet.gam4u.database.MyDatabase;
import pacmanemoticon.uet.gam4u.model.Song;

public class SongActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener, AdapterView.OnItemClickListener {
    private DrawerLayout drawer;

    private ListView listSong1;
    private ListView listSong2;
    private ListView listSong3;

    private ListSongAdapter listSongAdapter;
    private ListSongAdapter favoriteSongAdapter;
    private ListSongAdapter songSinger;

    private ArrayList<Song> songArrayList;
    private ArrayList<Song> songFavoriteList;
    private ArrayList<Song> songSingerList;
    private String key;

    private int mode;
    MyDatabase database;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_song);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final AlertDialog dialog = new AlertDialog.Builder(SongActivity.this, android.support.design.R.style.Base_Theme_AppCompat_Light_Dialog_Alert).setView(R.layout.dialog_search).show();
                EditText input = (EditText) dialog.findViewById(R.id.etSearch);
                key = input.getText().toString();
                Button btnOk, btnCancel;
                btnOk = (Button) dialog.findViewById(R.id.btnOk);
                btnCancel = (Button) dialog.findViewById(R.id.btnCancel);
                btnOk.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(SongActivity.this, SearchActivity.class);
                        intent.putExtra("keyword", key);
                        startActivity(intent);
                    }
                });
                btnCancel.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                    }
                });
            }
        });
        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close) {
            @Override
            public void onDrawerClosed(View drawerView) {
                super.onDrawerClosed(drawerView);
            }

            @Override
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
            }

            @Override
            public void onDrawerSlide(View drawerView, float slideOffset) {
                super.onDrawerSlide(drawerView, slideOffset);
            }

            @Override
            public void onDrawerStateChanged(int newState) {
                super.onDrawerStateChanged(newState);
            }
        };
        toggle.setDrawerIndicatorEnabled(true);
        toggle.syncState();
        drawer.setDrawerListener(toggle);
        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        Bundle bundle = getIntent().getExtras();
        String title = bundle.getString("title");
        mode = bundle.getInt("mode");
        getSupportActionBar().setTitle(title);
        try {
            database = new MyDatabase(this).open();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        database.createSampleData();
        listSong1 = (ListView) findViewById(R.id.list_song1);
        listSong2 = (ListView) findViewById(R.id.list_song2);
        listSong3 = (ListView) findViewById(R.id.list_song3);

        songArrayList = database.getSongWithAuthor();
        songFavoriteList = database.getFavoriteSong();
        songSingerList = database.getSongByArtistName(bundle.getString("singer_name"));

        listSongAdapter = new ListSongAdapter(this, songArrayList);
        favoriteSongAdapter = new ListSongAdapter(this, songFavoriteList);
        songSinger = new ListSongAdapter(this, songSingerList);

        listSong1.setAdapter(listSongAdapter);
        listSong2.setAdapter(favoriteSongAdapter);
        listSong3.setAdapter(songSinger);

        createViewByMode(mode);

        listSong1.setOnItemClickListener(this);
        listSong2.setOnItemClickListener(this);
        listSong3.setOnItemClickListener(this);
    }

    private void createViewByMode(int mode) {
        if (mode == 0) {
            listSong1.setVisibility(View.VISIBLE);
            listSong2.setVisibility(View.GONE);
            listSong3.setVisibility(View.GONE);
            getSupportActionBar().setTitle("Danh sách bài hát");
        } else if (mode == 1) {
            listSong1.setVisibility(View.GONE);
            listSong2.setVisibility(View.VISIBLE);
            listSong3.setVisibility(View.GONE);
            getSupportActionBar().setTitle("Bài hát yêu thích");
        } else {
            listSong1.setVisibility(View.GONE);
            listSong2.setVisibility(View.GONE);
            listSong3.setVisibility(View.VISIBLE);
            getSupportActionBar().setTitle("Bài hát của ca sĩ");
        }
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        Intent intent;
        switch (item.getItemId()) {
            case R.id.navHomePage:
                intent = new Intent(SongActivity.this, MainActivity.class);
                startActivity(intent);
                break;
            case R.id.navSongLibrary:
                if (mode != 0) mode = 0;
                createViewByMode(mode);
                break;
            case R.id.navArtist:
                intent = new Intent(SongActivity.this, ArtistActivity.class);
                startActivity(intent);
                break;
            case R.id.navGam:
                intent = new Intent(SongActivity.this, ChordMenuActivity.class);
                startActivity(intent);
                break;
            case R.id.navFavorite:
                if (mode == 0) mode = 1;
                createViewByMode(mode);
                break;
            case R.id.navSetting: {
            }
            case R.id.navLogin:
                intent = new Intent(this, LoginActivity.class);
                startActivity(intent);
                break;
        }
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        Song song = new Song();
        switch (mode) {
            case 0:
                song = songArrayList.get(position);
                break;
            case 1:
                song = songFavoriteList.get(position);
                break;
            case 2:
                song = songSingerList.get(position);
        }
        Intent intent = new Intent(SongActivity.this, SongContentActivity.class);
        intent.putExtra("title", song.getTitle());
        intent.putExtra("content", song.getContent());
        intent.putExtra("artist_name", song.getAuthors());
        intent.putExtra("link", song.link);
        startActivity(intent);
    }
}
