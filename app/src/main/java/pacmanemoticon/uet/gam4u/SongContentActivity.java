package pacmanemoticon.uet.gam4u;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.TextView;

import com.dqt.libs.chorddroid.components.ChordTextureView;

import org.w3c.dom.Text;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;

import pacmanemoticon.uet.gam4u.adapter.ListSongAdapter;
import pacmanemoticon.uet.gam4u.adapter.ListSongInContent;
import pacmanemoticon.uet.gam4u.database.MyDatabase;
import pacmanemoticon.uet.gam4u.database.SongDatabase;
import pacmanemoticon.uet.gam4u.model.Song;

/**
 * Created by Nam on 12/1/2015.
 */
public class SongContentActivity extends AppCompatActivity implements SeekBar.OnSeekBarChangeListener{
    Toolbar toolbar;
    private TextView tvTitle;
    private CheckBox btnLike;
    private ImageButton btnPlay;
    private TextView tvContent;
    private TextView tvArtist;
    private TextView tvSinger;
    private TextView tvMelody;
    private TextView tvSameArtist;
    private TextView tvNotHaveSameArtist;
    private TextView tvMusic;
    private LinearLayout chordLayout;
    private LinearLayout playMusic;
    private TextView btnChords;
    private ImageButton btnPlayMusic;
    private ImageButton btnPauseMusic;
    private MediaPlayer mediaPlayer;
    private SeekBar seekBar;
    private Handler mHandler = new Handler();
    private Utilities utils;
    private int seekForwardTime = 5000; // 5000 milliseconds
    private int seekBackwardTime = 5000; // 5000 milliseconds

    ArrayList<Song> curSong;
    ArrayList<String> artistName;
    ArrayList<Song> songSameArtist;

    ListView listSongSameView;
    ListSongInContent listSongInContent;
    MyDatabase database;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_song_content);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        toolbar.setNavigationIcon(getResources().getDrawable(R.mipmap.ic_left_arrow));
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        tvTitle = (TextView) findViewById(R.id.tvTitle);
        tvContent = (TextView) findViewById(R.id.tvContent);
        tvArtist = (TextView) findViewById(R.id.tvArtistName);
        tvSinger = (TextView) findViewById(R.id.tvSingerName);
        tvMelody = (TextView) findViewById(R.id.tvMelody);

//        ArrayList<Song> curSong = new ArrayList<Song>();
//        ArrayList<String> artistName = new ArrayList<String>();
//        ArrayList<Song> songSameArtist = new ArrayList<Song>();
//
        Bundle bundle = getIntent().getExtras();
        String title = bundle.getString("title");
        String artist_name = bundle.getString("artist_name");
        String song_link = bundle.getString("link");
        getSupportActionBar().setTitle(title);
        try {
            database = new MyDatabase(this).open();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        database.createSampleData();
        curSong = database.getSongByName(title);
        artistName = database.getArtistBySongId(curSong.get(0).songId);

        tvArtist.setText(artistName.get(0));
        tvSinger.setText(artistName.get(0));
        tvMelody.setText(curSong.get(0).rhythm);
        tvTitle.setText(title);
        String content = bundle.getString("content");
        String editedContent = "";
        //String content = "<font color='#0066FF'>red</font>";
        String[] line = content.split("\n");
        for(String l : line){
            ArrayList<Integer> pos = filterPositionChord(l);
            ArrayList<String> chord = filterChord(l);
            String lineChord = "";
            if(!chord.isEmpty()){
                //editedContent += ;
                editedContent += "<font color='#0066FF'>"+repeat("&nbsp;", pos.get(0)) + chord.get(0)+"</font>";
                if(chord.size() > 1){
                    for(int i = 1; i< chord.size(); i++){
                        //editedContent += ;
                        editedContent += "<font color='#0066FF'>"+repeat("&nbsp;", pos.get(i) - pos.get(i - 1)) + chord.get(i)+"</font>";
                    }
                }

                editedContent += "<br></br>";
            }
            else{
                editedContent += "<br></br>";
            }

            int  i = 0;
            while(i < l.length()){
                if(l.charAt(i) == '['){
                    while(l.charAt(i) != ']'){
                        l = l.substring(0, i) + l.substring(i+1);
                    }
                    l = l.substring(0, i) + l.substring(i+1);
                }
                else i++;
            }
            l = l.trim();
            l = l.replaceAll("\\s+", " ");

            editedContent += l + "<br></br>";
        }
        tvContent.setText(Html.fromHtml(editedContent));
        songSameArtist = database.getSongByArtistName(artist_name);
        for(int i=0; i<songSameArtist.size(); i++){
            if(songSameArtist.get(i).getTitle().equals(title)){
                songSameArtist.remove(i);
            }
        }
        tvNotHaveSameArtist = (TextView) findViewById(R.id.tvSameSinger);
        if(songSameArtist.size() >= 1) tvNotHaveSameArtist.setVisibility(View.GONE);
        else{
            tvNotHaveSameArtist.setVisibility(View.VISIBLE);
        }
        listSongSameView = (ListView) findViewById(R.id.list_song_same_singer);
        listSongInContent = new ListSongInContent(this, songSameArtist);
        listSongSameView.setAdapter(listSongInContent);
        tvSameArtist = (TextView) findViewById(R.id.same_singer_btn);
        setListViewHeightBasedOnChildren(listSongSameView);
        listSongSameView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Song song = songSameArtist.get(position);
                Intent intent = new Intent(SongContentActivity.this, SongContentActivity.class);
                intent.putExtra("title", song.getTitle());
                intent.putExtra("content", song.getContent());
                intent.putExtra("artist_name", song.getAuthors());
                intent.putExtra("link", song.link);
                startActivity(intent);
            }
        });
        chordLayout = (LinearLayout) findViewById(R.id.listChords);
        final ArrayList<String> chords = filterChord(content);
        ArrayList<String> filterChords = new ArrayList<String>();
        for(String c: chords){
            if(!filterChords.contains(c)){
                filterChords.add(c);
            }
        }
        for(String c: filterChords){
            ChordTextureView chordView = new ChordTextureView(this);
            chordView.drawChord(c);
            //chordView.setLayoutParams();
            chordLayout.addView(chordView, 200, 200);
        }

        btnChords =(TextView) findViewById(R.id.tvChords);
        btnChords.setText("Ẩn");
        btnChords.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (chordLayout.getVisibility() == View.VISIBLE) {
                    btnChords.setText("Hợp âm được sử dụng");
                    chordLayout.setVisibility(View.GONE);
                } else {
                    btnChords.setText("Ẩn");
                    chordLayout.setVisibility(View.VISIBLE);
                }
            }
        });
        playMusic = (LinearLayout) findViewById(R.id.liLayoutPlayMusic);
        tvMusic = (TextView) findViewById(R.id.tvPlayMusic);
        btnPlayMusic = (ImageButton) findViewById(R.id.btnPlayMusic);
        seekBar = (SeekBar) findViewById(R.id.seekBar);
        tvMusic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (playMusic.getVisibility() == View.VISIBLE) {
                    tvMusic.setText("Nghe bài hát");
                    playMusic.setVisibility(View.GONE);
                } else {
                    tvMusic.setText("Ẩn");
                    playMusic.setVisibility(View.VISIBLE);
                }
            }
        });
        mediaPlayer = new MediaPlayer();
        utils = new Utilities();
        mediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
        Uri myUri = Uri.parse(song_link); // initialize Uri here
        try {
            mediaPlayer.setDataSource(getApplicationContext(), myUri);
        } catch (IOException e) {
            e.printStackTrace();
        }
        try {
            mediaPlayer.prepare();
        } catch (IOException e) {
            e.printStackTrace();
        }
        btnPlayMusic.setImageResource(R.mipmap.ic_play);
        btnPlayMusic.setColorFilter(Color.rgb(205, 205, 193));
        btnPlayMusic.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if(!mediaPlayer.isPlaying()){
                            mediaPlayer.start();
                            btnPlayMusic.setImageResource(R.mipmap.ic_pause);
                            btnPlayMusic.setColorFilter(Color.rgb(205, 205, 193));
                            updateProgressBar();
                        }
                        else{
                            mediaPlayer.pause();
                            btnPlayMusic.setImageResource(R.mipmap.ic_play);
                            btnPlayMusic.setColorFilter(Color.rgb(205, 205, 193));
                        }

                    }
                }
        );

        seekBar.setOnSeekBarChangeListener(this);
    }

    public ArrayList<String> filterChord(String content){
        ArrayList<String> line = new ArrayList<String>();
        int i = 0;
        while( i < content.length()){
            if(content.charAt(i) == '['){
                i++;
                String a = "";
                while(content.charAt(i) != ']'){
                    a += content.charAt(i);
                    i++;
                }
                line.add(a);
            }
            else i++;
        }
        return line;
    }

    public ArrayList<Integer> filterPositionChord(String content){
        ArrayList<Integer> line = new ArrayList<Integer>();
        int i = 0;
        while( i < content.length()){
            if(content.charAt(i) == '['){
                line.add(i);
                i++;
            }
            else i++;
        }
        return line;
    }

    public String repeat(String str, int times) {
        return new String(new char[times]).replace("\0", str);
    }


    public static void setListViewHeightBasedOnChildren(ListView listView) {
        ListAdapter listAdapter = listView.getAdapter();
        if (listAdapter == null) {
            // pre-condition
            return;
        }

        int totalHeight = 0;
        for (int i = 0; i < listAdapter.getCount(); i++) {
            View listItem = listAdapter.getView(i, null, listView);
            listItem.measure(0, 0);
            totalHeight += listItem.getMeasuredHeight();
        }

        ViewGroup.LayoutParams params = listView.getLayoutParams();
        params.height = totalHeight + (listView.getDividerHeight() * (listAdapter.getCount() - 1));
        listView.setLayoutParams(params);
        listView.requestLayout();
    }

    /**
     * Update timer on seekbar
     * */
    public void updateProgressBar() {
        mHandler.postDelayed(mUpdateTimeTask, 100);
    }

    /**
     * Background Runnable thread
     * */
    private Runnable mUpdateTimeTask = new Runnable() {
        public void run() {
            long totalDuration = mediaPlayer.getDuration();
            long currentDuration = mediaPlayer.getCurrentPosition();

            // Updating progress bar
            int progress = (int)(utils.getProgressPercentage(currentDuration, totalDuration));
            //Log.d("Progress", ""+progress);
            seekBar.setProgress(progress);

            // Running this thread after 100 milliseconds
            mHandler.postDelayed(this, 100);
        }
    };

    /**
     *
     * */
    @Override
    public void onProgressChanged(SeekBar seekBar, int progress, boolean fromTouch) {

    }

    /**
     * When user starts moving the progress handler
     * */
    @Override
    public void onStartTrackingTouch(SeekBar seekBar) {
        // remove message Handler from updating progress bar
        mHandler.removeCallbacks(mUpdateTimeTask);
    }

    /**
     * When user stops moving the progress hanlder
     * */
    @Override
    public void onStopTrackingTouch(SeekBar seekBar) {
        mHandler.removeCallbacks(mUpdateTimeTask);
        int totalDuration = mediaPlayer.getDuration();
        int currentPosition = utils.progressToTimer(seekBar.getProgress(), totalDuration);

        // forward or backward to certain seconds
        mediaPlayer.seekTo(currentPosition);

        // update timer progress again
        updateProgressBar();
    }

}
