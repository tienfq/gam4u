package pacmanemoticon.uet.gam4u.adapter;

import android.app.Activity;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import org.w3c.dom.Text;

import java.net.URI;
import java.net.URL;
import java.util.ArrayList;

import pacmanemoticon.uet.gam4u.R;
import pacmanemoticon.uet.gam4u.model.Artist;
import pacmanemoticon.uet.gam4u.model.Song;

/**
 * Created by anhdt on 1/1/2016.
 */
public class ListArtistAdapter extends ArrayAdapter<Artist>{
    private Activity activity;
    private ArrayList<Artist> artistArrayList;

    private ImageView imgArtist;
    private TextView txtArtistName;
    private TextView quantitySong;

    public ListArtistAdapter(Activity activity,ArrayList<Artist> artistArrayList){
        super(activity, R.layout.item_list_artist,artistArrayList);
        this.activity = activity;
        this.artistArrayList = artistArrayList;
    }

    @Override
    public int getCount() {
        return artistArrayList.size();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null){
            LayoutInflater inflater = activity.getLayoutInflater();
            convertView = inflater.inflate(R.layout.item_list_artist,parent,false);
            imgArtist = (ImageView) convertView.findViewById(R.id.imgArtist);
            txtArtistName = (TextView) convertView.findViewById(R.id.txtArtistName);
            quantitySong = (TextView) convertView.findViewById(R.id.quantitySong);
            Artist artist = artistArrayList.get(position);
            Uri url = Uri.parse("android.resource://pacmanemoticon.uet.gam4u/raw/artist" + artist.getArtistId());
            imgArtist.setImageURI(url);
            if(imgArtist.getDrawable() == null){
                imgArtist.setImageResource(R.drawable.noname);
            }
            txtArtistName.setText(artist.getArtistName());
            quantitySong.setText(artist.getQuantitySong() + " bài hát");
        }
        return convertView;
    }
}
