package pacmanemoticon.uet.gam4u.adapter;

import android.app.Activity;
import android.support.design.widget.Snackbar;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import java.sql.SQLException;
import java.util.ArrayList;

import pacmanemoticon.uet.gam4u.R;
import pacmanemoticon.uet.gam4u.database.MyDatabase;
import pacmanemoticon.uet.gam4u.model.Song;

/**
 * Created by anhdt on 12/28/2015.
 */
public class ListSongInContent extends ArrayAdapter<Song> {
    private Activity activity;
    private ArrayList<Song> songArrayList;

    private TextView tvTitle;
    private TextView tvContent;

    public ListSongInContent(Activity activity,ArrayList<Song> songArrayList){
        super(activity,R.layout.item_list_song,songArrayList);
        this.activity = activity;
        this.songArrayList = songArrayList;
    }

    @Override
    public int getCount() {
        return songArrayList.size();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null){
            LayoutInflater inflater = activity.getLayoutInflater();
            convertView = inflater.inflate(R.layout.item_in_content,parent,false);
            tvTitle = (TextView) convertView.findViewById(R.id.tvTitle);
            tvContent = (TextView) convertView.findViewById(R.id.tvContent);
            Song song = songArrayList.get(position);
            tvTitle.setText(song.getTitle());
            String tmp = "";
            for (int i = 0; i < song.getFirstLyric().length(); i++){

                if(song.getFirstLyric().charAt(i)=='['){
                    while(song.getFirstLyric().charAt(i) != ']'){
                        tmp += "<font color='#0066FF'>"+song.getFirstLyric().charAt(i)+"</font>";
                        i++;
                    }
                    tmp += "<font color='#0066FF'>"+song.getFirstLyric().charAt(i)+"</font>";
                }
                else{
                    tmp += song.getFirstLyric().charAt(i);
                }
            }
            tvContent.setText(Html.fromHtml(tmp));

        }
        return convertView;
    }
}
