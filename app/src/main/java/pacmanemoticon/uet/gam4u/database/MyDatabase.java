package pacmanemoticon.uet.gam4u.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import pacmanemoticon.uet.gam4u.model.Artist;
import pacmanemoticon.uet.gam4u.model.Song;
import pacmanemoticon.uet.gam4u.model.Chord;
import java.lang.reflect.Array;
import java.sql.SQLException;
import java.text.Normalizer;
import java.util.ArrayList;
import java.util.Date;
import java.util.Vector;
import java.util.regex.Pattern;


/**
 * Created by anhdt on 11/29/2015.
 */
public class MyDatabase {
    /* database name */
    private static final String DATABASE_NAME = "Gam4u";

    /* version database*/
    private static final int DATABASE_VERSION = 8;

    /* Label name and colomns in database*/
    private static final String TABLE_SONG = "SongTbl";
    private static final String TABLE_CHORD = "ChordTbl";
    private static final String TABLE_PLAYLIST = "PlaylistTbl";
    private static final String TABLE_ARTIST = "ArtistTbl";

    /*Some Objects are used for link database*/
    private static Context context;
    static SQLiteDatabase db;
    private OpenHelper openHelper;

    /*Contructor Object MyDatabase*/
    public MyDatabase(Context c){
        MyDatabase.context = c;
    }

    /*Open connection with database*/
    public MyDatabase open() throws SQLException{
        openHelper = new OpenHelper(context);
        db = openHelper.getWritableDatabase();
        return this;
    }

    /*Close connection with database*/
    public void close(){
        openHelper.close();
    }

    public long insertData(String table, Vector cols, Vector data){
        ContentValues cv = new ContentValues();
        for(int i = 0; i < cols.size(); i++){
            cv.put(cols.get(i).toString(), data.get(i).toString());
        }
        return db.insert(table, null, cv);
    }

    public long insertArtist(int id, String name, String ascii){
        Vector a = new Vector();
        a.add("artist_id");
        a.add("artist_name");
        a.add("artist_ascii");
        Vector b = new Vector();
        b.add(id);
        b.add(name);
        b.add(ascii);
        return insertData("ArtistTbl", a, b);
    }

    public long insertSong(int id, String song_title, String song_link, String song_content, String song_first_lyric, String song_date, String song_title_ascii, int song_lastview, int song_favorite, String song_rhythm){
        Vector a = new Vector();
        a.add("song_id");
        a.add("song_title");
        a.add("song_link");
        a.add("song_content");
        a.add("song_first_lyric");
        a.add("song_date");
        a.add("song_title_ascii");
        a.add("song_lastview");
        a.add("song_isfavorite");
        a.add("song_rhythm");
        Vector b = new Vector();
        b.add(id);
        b.add(song_title);
        b.add(song_link);
        b.add(song_content);
        b.add(song_first_lyric);
        b.add(song_date);
        b.add(song_title_ascii);
        b.add(song_lastview);
        b.add(song_favorite);
        b.add(song_rhythm);
        return insertData("SongTbl", a, b);
    }

    public long insertChord(int id, String name, String relations){
        Vector a = new Vector();
        a.add("chord_id");
        a.add("chord_name");
        a.add("chord_relations");
        Vector b = new Vector();
        b.add(id);
        b.add(name);
        b.add(relations);
        return insertData("ChordTbl", a, b);
    }

    public long insertPlaylist(int id, String name){
        Vector a = new Vector();
        a.add("playlist_id");
        a.add("playlist_name");
        Vector b = new Vector();
        b.add(id);
        b.add(name);
        return insertData("Playlist_Tbl", a, b);
    }

    public long insertPlaylistSong(int playlist_id, int song_id){
        Vector a = new Vector();
        a.add("playlist_id");
        a.add("song_id");
        Vector b = new Vector();
        b.add(playlist_id);
        b.add(song_id);
        return insertData("Playlist_Song_Tbl", a, b);
    }

    public long insertSongAuthor(int song_id, int artist_id){
        Vector a = new Vector();
        a.add("song_id");
        a.add("artist_id");
        Vector b = new Vector();
        b.add(song_id);
        b.add(artist_id);
        return insertData("Song_Author_Tbl", a, b);
    }

    public long insertSongChord(int song_id, int chord_id){
        Vector a = new Vector();
        a.add("song_id");
        a.add("chord_id");
        Vector b = new Vector();
        b.add(song_id);
        b.add(chord_id);
        return insertData("Song_Chord_Tbl", a, b);
    }

    public long insertSongSinger(int song_id, int artist_id){
        Vector a = new Vector();
        a.add("song_id");
        a.add("artist_id");
        Vector b = new Vector();
        b.add(song_id);
        b.add(artist_id);
        return insertData("Song_Singer_Tbl", a, b);
    }

    public void createSampleData(){
        insertArtist(1, "Tấn Minh", "tan minh");
        insertSong(1,
                "Bức thư tinh thứ nhất",
                "http://data16.chiasenhac.com/downloads/1006/1/1005865-84de95ae/128/Buc%20Thu%20Tinh%20Dau%20Tien%20-%20Tan%20Minh%20[MP3%20128kbps].mp3",
                "[C]Khi anh đưa mắt nhìn em qua tấm [Em]gương\n" +
                        "Ta đã gặp [Am]nhau, bối rối thật [F]lâu\n" +
                        "Đêm nay [C]dường như những ánh [F]mắt\n" +
                        "Muốn [G]đi kiếm tìm nhau\n" +
                        "[C]Anh muốn nói với em những điều thật lớn [Em]lao.... [Bb]\n" +
                        "[F]Sẽ luôn ở đây, nơi tim [Em]anh, tình yêu bất [F]tận\n" +
                        "Phút giây [Em]anh nghẹn [Am]lời\n" +
                        "Vì biết [Bb]em yêu anh [G]\n" +
                        "Và anh [F], sẽ là người đàn ông của đời [G]em\n" +
                        "Anh đã mơ [F]về ngôi nhà [G]và những đứa trẻ [C]\n" +
                        "Vì yêu [F]em, ngày mai [G]anh thêm vững bước [Em]trên con đường dài [Am]\n" +
                        "Em [G]có [F]nghe [Em]mùa đông [Fm]như những ngọn đèn vàng\n" +
                        "Anh nhớ [C]em [Em] [F] [G]\n" +
                        "Anh nhớ em\n" +
                        "Miên man [F]bên trang thư tình [Em]gửi đến em [Am]\n" +
                        "Bạn đời [Fm]ơi\n" +
                        "Anh mơ một [Em]sớm tỉnh giấc\n" +
                        "Mắt anh kiếm [F]tìm\n" +
                        "Tai anh lắng [Em]nghe\n" +
                        "Môi [F]anh cất tiếng gọi [Em]\n" +
                        "Và vòng [Dm]tay anh rộng mở [G]\n" +
                        "Đón em vào [C]lòng",
                "[C]Khi anh đưa mắt nhìn em qua tấm [Em]gương\n" +
                        "Ta đã",
                "",
                "buc thu tinh thu nhat",
                0,
                0,
                "ballad");
        insertSongAuthor(1, 1);

        insertArtist(2, "Anh Khang", "anh khang");

        insertSong(2,
                "Anh mơ",
                "http://data16.chiasenhac.com/downloads/1008/2/1007313-ea11a578/128/Anh%20Mo%20-%20Anh%20Khang%20[MP3%20128kbps].mp3",
                "[C]Sáng nay anh được... [G]gặp em, \n" +
                        "Lòng anh hạnh [Am]phúc thật nhiều...  [Em]\n" +
                        "Được nghe em [F]nói thật nhẹ nhàng biết [C]mấy, \n" +
                        "Và giây phút [F]này mong sao ngừng [G]trôi...\n" +
                        "Đã bao thời gian anh mơ, \n" +
                        "Được nhìn em hé nụ cười... \n" +
                        "Người ơi em có... hiểu được anh không?\n" +
                        "Lòng anh thẫn thờ yêu em nhiều lắm!\n" +
                        "\n" +
                        "Ước mong thời gian[C]... sẽ quay trở [G]lại, \n" +
                        "Để anh được [Am]nói với em thêm một câu[Em]...\n" +
                        "Rằng ngày mai nếu[F]... có gặp lại nhau[C]...\n" +
                        "Thì em có [F]cần tay anh ôm em thật lâu[G]...\n" +
                        "Chúc em ngày mai[C]... sẽ luôn thật [G]vui...\n" +
                        "Ở nơi xa [Am]ấy có nắng ru tình em[Em]...\n" +
                        "Còn anh sẽ [F]nhớ, nhớ mãi nụ cười ấy[C]...\n" +
                        "Chờ nghe những [F]lời... yêu thương trìu mến[G]...\n" +
                        "Anh [C]mơ...",
                "[C]Sáng nay anh được... [G]gặp em,  Lòng anh hạnh ",
                "",
                "anh mo",
                0,
                1,
                "disco");
        insertSongAuthor(2, 2);

        insertArtist(3, "Nguyễn Đức Cường", "nguyen duc cuong");

        insertSong(3,
                "Bất chợt một tình yêu",
                "http://mp3.zing.vn/download/song/Bat-Chot-Mot-Tinh-Yeu-Nguyen-Duc-Cuong/kGJGtLmsVxcHpcJyZDxtFHZG",
                "Phố đông một ngày [Dm]nắng. [G]Khi đôi chân lang thang đi [Am]tìm nhau\n" +
                        "Bất chợt nụ [Dm]cười xinh, [G]em lung linh giữa bao khuôn [Am]mặt kia, yeah...\n" +
                        "Một tia [Dm]nắng vương trên làn [G]mi, vội [F]sao lăn bờ môi, \n" +
                        "đã muốn cất tiếng nói yêu [C]thương\n" +
                        "Phải tình [Dm]nhân anh đã làm quen, và [G]tiếc nuối những giấc mơ nồng [Em]cháy\n" +
                        "hu..hu.. [Em7]yeah...\n" +
                        "Điệp khúc:\n" +
                        "Tiếng yêu ban [F]đầu chợt [G]như vạt [C]nắng\n" +
                        "khát khao [F]gọi những ban [G]mai tình [Am]yêu\n" +
                        "Đừng vội [F]đến, đừng vội [G]đi, đừng vội [C]mất nhau\n" +
                        "Trái tim [F]này hát mãi [G]lời nồng [Am]say\n" +
                        "Có một [F]ngày đã bất [G]chợt tình [Am]yêu\n" +
                        "RAP:\n" +
                        "[Am]Nhưng có lúc yêu thương thật mong manh\n" +
                        "[F]Xin cho bao dung những giận hờn trong mắt nhau\n" +
                        "Một tình [G]yêu ta sẽ tìm đâu, mãi tìm đâu\n" +
                        "Cuộc tình [Am]bất chợt ngỡ làm ta mơ\n" +
                        "Nơi con tim [F]này mang cho anh những [G]rung động\n" +
                        "Khi tia [Am]nắng bướm hoa khẽ cười\n" +
                        "Làm ta bất chợt yêu. [Am]",
                "Phố đông một ngày [Dm]nắng. [G]Khi đôi chân lang t",
                "",
                "bat chot mot tinh yeu",
                0,
                0,
                "disco");
        insertSongAuthor(3, 3);

        insertSong(4,
                "Ai còn chờ ai",
                "http://nhacso.net/songs/download-song?songId=XVlXVEVf",
                "Chơi [G]vơi một [D]mình ai [G]\n" +
                        "Chơi [G]vơi đường [D]thật dài [G]\n" +
                        "Chơi [D]vơi... [Bm]nhớ [G]thương [D]hoài\n" +
                        "Biết [G]có... [A]ngày trở lại? [D]\n" +
                        "Sao [G]đêm... [D]dài thật dài? [G]\n" +
                        "Cho [G]anh còn [Bm]đợi [G]hoài\n" +
                        "Sao [D]em vẫn e ngại? [G]\n" +
                        "Để [G]ai còn chờ [D]ai... [A]\n" +
                        "Thương [Bm]nhau hãy quay [G]về\n" +
                        "Yêu [Bm]nhau hãy quay [Em]về\n" +
                        "Về ngồi sát [Am]kề\n" +
                        "Nhẹ [D]làn tóc thề [Am]\n" +
                        "như [D]ngày đầu [Am]tiên [D7]\n" +
                        "Thế nhé dấu [G]yêu\n" +
                        "Có nhớ đến [G]nhau\n" +
                        "Về đây [Em]đón [G]đưa\n" +
                        "Về nghiêng nắng [Em]mưa\n" +
                        "Cho [D]anh thương lại [G]\n" +
                        "Cho [Am]em yêu [G]lại\n" +
                        "Cho ta mơ về bình [Am]yên\n" +
                        "Thế nhé dấu yêu\n" +
                        "Có nhớ đến nhau\n" +
                        "Gọi [Em]em giấc [G]mơ\n" +
                        "Ngày [Em]yêu ấu [G]thơ\n" +
                        "Biết đến bao [Am]giờ\n" +
                        "Anh thôi mong [G]chờ\n" +
                        "Về [D]đây nhé em... [G]",
                "Chơi [G]vơi một [D]mình ai [G]\n",
                "",
                "ai con cho ai",
                0,
                1,
                "slowrock");
        insertSongAuthor(4, 2);

        insertArtist(4, "Minh Vương M4U", "minh vuong m4u");

        insertSong(5,
                "Quê hương Việt Nam",
                "http://data19.chiasenhac.com/downloads/1104/2/1103337-8e3bd1d2/128/Que%20Huong%20Viet%20Nam%20-%20Anh%20Khang_%20Suboi%20[MP3%20128kbps].mp3",
                "Một ngày [G]mới khi ánh nắng lên\n" +
                        "Gió khẽ [Cadd9]đưa trên bông lúa xanh\n" +
                        "Và tình [G]yêu trong tim có em gần bên [D]anh\n" +
                        "Dịu dàng anh [G]đưa em qua tháng năm\n" +
                        "Ở nơi [Cadd9]đây tràn đầy mến thương\n" +
                        "Gửi lời [G]yêu lắm ôi [D]quê hương Việt [G]Nam\n" +
                        "Mùa xuân [G]sang muôn hoa thắm tươi\n" +
                        "Khắp nơi [Cadd9]nơi rộn ràng tiếng cười\n" +
                        "Và hạ [G]sang mang theo khát khao màu nắng [D]mới\n" +
                        "Rồi mùa thu [G]qua cho bao ước mơ\n" +
                        "Mỗi sớm [Cadd9]chiều cùng nhìn lá rơi\n" +
                        "Gửi lời [G]yêu lắm ôi [D]quê hương Việt [G]Nam\n" +
                        "Yêu [G]lắm nơi này...cho bao nhiêu yêu thương ta trao nhau\n" +
                        "Yêu [Cadd9]lắm nơi này...từng ngày rộn ràng trôi qua mau\n" +
                        "Yêu [G]lắm những gì...nhẹ trôi êm [D]đêm\n" +
                        "Yêu [G]lắm nơi này...cho bao nhiêu yêu thương anh trao em\n" +
                        "Yêu [Cadd9]lắm nơi này...từng ngày rộn ràng trôi qua thêm\n" +
                        "Và gửi lời [G]yêu lắm ôi [D]quê hương Việt [G]Nam",
                "Một ngày [G]mới khi ánh nắng lên",
                "",
                "que huong viet nam",
                0,
                1,
                "ballad");
        insertSongAuthor(5, 2);
    }

    public ArrayList<Song> getListSong(){
        ArrayList<Song> listSong = new ArrayList<>();
        String[] cols = {"song_id", "song_title", "song_link", "song_content", "song_first_lyric", "song_date", "song_title_ascii", "song_lastview", "song_isfavorite", "song_rhythm"};
        Cursor c = db.query("SongTbl", cols, null, null, null, null, null);
        if(c==null)
            Log.v("Cursor", "C is NULL");

        int song_id = c.getColumnIndex("song_id");
        int song_title = c.getColumnIndex("song_title");
        int song_link = c.getColumnIndex("song_link");
        int song_content = c.getColumnIndex("song_content");
        int song_first_lyric = c.getColumnIndex("song_first_lyric");
        int song_date = c.getColumnIndex("song_date");
        int song_title_ascii = c.getColumnIndex("song_title_ascii");
        int song_lastview = c.getColumnIndex("song_lastview");
        int song_isfavorite = c.getColumnIndex("song_isfavorite");
        int song_rhythm = c.getColumnIndex("song_rhythm");

        for(c.moveToFirst(); !c.isAfterLast(); c.moveToNext()){
            listSong.add(new Song(Integer.parseInt(c.getString(song_id)), c.getString(song_title), c.getString(song_link), c.getString(song_content), c.getString(song_first_lyric), new Date(), c.getString(song_title_ascii), Integer.parseInt(c.getString(song_lastview)), Integer.parseInt(c.getString(song_isfavorite))  , c.getString(song_rhythm), getArtistBySongId(Integer.parseInt(c.getString(song_id))).get(0)));
        }

        c.close();
        return listSong;
    }

    public ArrayList<Song> getSongByName(String name){
        /*Remove Accents of Unicode*/
        String nfdNormalizedString = Normalizer.normalize(name, Normalizer.Form.NFD);
        Pattern pattern = Pattern.compile("\\p{InCombiningDiacriticalMarks}+");
        name = pattern.matcher(nfdNormalizedString).replaceAll("");

        ArrayList<Song> tmp = new ArrayList<>();
        String[] columns = {"song_id", "song_title", "song_link", "song_content", "song_first_lyric", "song_date", "song_title_ascii", "song_lastview", "song_isfavorite", "song_rhythm"};
        String []selectionArgs = {"%" + name + "%"};
        Cursor c = db.query("SongTbl", columns, "song_title_ascii LIKE ?", selectionArgs, null, null, null);

        int song_id = c.getColumnIndex("song_id");
        int song_title = c.getColumnIndex("song_title");
        int song_link = c.getColumnIndex("song_link");
        int song_content = c.getColumnIndex("song_content");
        int song_first_lyric = c.getColumnIndex("song_first_lyric");
        int song_date = c.getColumnIndex("song_date");
        int song_title_ascii = c.getColumnIndex("song_title_ascii");
        int song_lastview = c.getColumnIndex("song_lastview");
        int song_isfavorite = c.getColumnIndex("song_isfavorite");
        int song_rhythm = c.getColumnIndex("song_rhythm");

        for(c.moveToFirst(); !c.isAfterLast(); c.moveToNext()){
            tmp.add(new Song(Integer.parseInt(c.getString(song_id)), c.getString(song_title), c.getString(song_link), c.getString(song_content), c.getString(song_first_lyric), new Date(), c.getString(song_title_ascii), Integer.parseInt(c.getString(song_lastview)), Integer.parseInt(c.getString(song_isfavorite))  , c.getString(song_rhythm), getArtistBySongId(Integer.parseInt(c.getString(song_id))).get(0)));
        }
        c.close();
        return tmp;
    }

    public ArrayList<Integer> getIdByArtistName(String name){
        /*Remove Accents of Unicode*/
        String nfdNormalizedString = Normalizer.normalize(name, Normalizer.Form.NFD);
        Pattern pattern = Pattern.compile("\\p{InCombiningDiacriticalMarks}+");
        name = pattern.matcher(nfdNormalizedString).replaceAll("");

        ArrayList<Integer> tmp = new ArrayList<>();
        String[] columns = {"_id", "artist_id", "artist_name", "artist_ascii"};
        String []selectionArgs = {"%" + name + "%"};
        Cursor c = db.query("ArtistTbl", columns, "artist_ascii LIKE ?", selectionArgs, null, null, null);

        int iT = c.getColumnIndex("artist_id");

        for(c.moveToFirst(); !c.isAfterLast(); c.moveToNext()){
            tmp.add(Integer.parseInt(c.getString(iT)));
        }
        c.close();
        return tmp;
    }

    public Song getSongById(int id){
        Song tmp = new Song();
        String[] cols = {"song_id", "song_title", "song_link", "song_content", "song_first_lyric", "song_date", "song_title_ascii", "song_lastview", "song_isfavorite", "song_rhythm"};
        //String []selectionArgs = {"%" + id + "%"};
        Cursor c = db.query("SongTbl", cols, "song_id = " + id, null, null, null, null);

        int song_id = c.getColumnIndex("song_id");
        int song_title = c.getColumnIndex("song_title");
        int song_link = c.getColumnIndex("song_link");
        int song_content = c.getColumnIndex("song_content");
        int song_first_lyric = c.getColumnIndex("song_first_lyric");
        int song_date = c.getColumnIndex("song_date");
        int song_title_ascii = c.getColumnIndex("song_title_ascii");
        int song_lastview = c.getColumnIndex("song_lastview");
        int song_isfavorite = c.getColumnIndex("song_isfavorite");
        int song_rhythm = c.getColumnIndex("song_rhythm");

        for(c.moveToFirst(); !c.isAfterLast(); c.moveToNext()){
            tmp = new Song(Integer.parseInt(c.getString(song_id)), c.getString(song_title), c.getString(song_link), c.getString(song_content), c.getString(song_first_lyric), new Date(), c.getString(song_title_ascii), Integer.parseInt(c.getString(song_lastview)), Integer.parseInt(c.getString(song_isfavorite))  , c.getString(song_rhythm), getArtistBySongId(Integer.parseInt(c.getString(song_id))).get(0));
        }
        c.close();
        return tmp;
    }

    public String getArtistById(int id){
        String[] cols = {"_id", "artist_id", "artist_name", "artist_ascii"};
        Cursor c = db.query("ArtistTbl", cols, "artist_id =" + id, null, null, null, null);
        int iT = c.getColumnIndex("artist_name");
        if(c != null){
            String tmp = "";
            for(c.moveToFirst(); !c.isAfterLast(); c.moveToNext()){
                tmp = c.getString(iT);
            }
            return tmp;
        }
        return null;
    }

    public ArrayList<Song> getSongByArtist(String name){
        ArrayList<Integer> idArtist = getIdByArtistName(name);
        ArrayList<Song> song = new ArrayList<>();
        String[] columns = {"_id", "song_id", "artist_id"};
        for(int id : idArtist){
            Cursor c = db.query("Song_Author_Tbl", columns, "artist_id = " + id, null, null, null, null);
            int iT = c.getColumnIndex("song_id");
            int tmp = 0;
            for(c.moveToFirst(); !c.isAfterLast(); c.moveToNext()){
                tmp = Integer.parseInt(c.getString(iT));
                if(!song.contains(getSongById(tmp))) song.add(getSongById(tmp));
            }
        }
        return song;
    }


    public ArrayList<Song> getFavoriteSong(){
        ArrayList<Song> songs = new ArrayList<>();
        String[] cols = {"song_id", "song_title", "song_link", "song_content", "song_first_lyric", "song_date", "song_title_ascii", "song_lastview", "song_isfavorite", "song_rhythm"};
        Cursor c = db.query("SongTbl", cols, "song_isfavorite = 1", null, null, null, null);
        int song_id = c.getColumnIndex("song_id");
        int song_title = c.getColumnIndex("song_title");
        int song_link = c.getColumnIndex("song_link");
        int song_content = c.getColumnIndex("song_content");
        int song_first_lyric = c.getColumnIndex("song_first_lyric");
        int song_date = c.getColumnIndex("song_date");
        int song_title_ascii = c.getColumnIndex("song_title_ascii");
        int song_lastview = c.getColumnIndex("song_lastview");
        int song_isfavorite = c.getColumnIndex("song_isfavorite");
        int song_rhythm = c.getColumnIndex("song_rhythm");

        for(c.moveToFirst(); !c.isAfterLast(); c.moveToNext()){
            songs.add(new Song(Integer.parseInt(c.getString(song_id)), c.getString(song_title), c.getString(song_link), c.getString(song_content), c.getString(song_first_lyric), new Date(), c.getString(song_title_ascii), Integer.parseInt(c.getString(song_lastview)), Integer.parseInt(c.getString(song_isfavorite))  , c.getString(song_rhythm), getArtistBySongId(Integer.parseInt(c.getString(song_id))).get(0)));
        }
        c.close();
        return songs;
    }

    public ArrayList<Integer> getArtistIdBySongId(int id){
        ArrayList<Integer> tmp = new ArrayList<>();
        String[] cols = {"_id", "song_id", "artist_id"};
        Cursor c = db.query("Song_Author_Tbl", cols, "song_id = " + id, null, null, null, null);
        int iT = c.getColumnIndex("artist_id");
        if(c != null){
            for (c.moveToFirst(); !c.isAfterLast(); c.moveToNext()){
                tmp.add(Integer.parseInt(c.getString(iT)));
            }
            return tmp;
        }
        c.close();
        return null;
    }

    public ArrayList<String> getArtistBySongId(int id){
        ArrayList<String> tmp = new ArrayList<>();
        ArrayList<Integer> a = getArtistIdBySongId(id);
        for(int i : a){
            tmp.add(getArtistById(i));
        }
        return tmp;
    }

    public ArrayList<Song> getSongByArtistName(String name){
        /*Remove Accents of Unicode*/
        ArrayList<Song> listSong = getListSong();
        ArrayList<Song> songSameArtist = new ArrayList<Song>();
        for(Song s : listSong){
            if(s.getAuthors().equals(name)){
                songSameArtist.add(s);
            }
        }
        return songSameArtist;
    }

    public ArrayList<Song> getSongWithAuthor(){
        ArrayList<Song> songs = new ArrayList<>();
        String[] cols = {"song_id", "song_title", "song_link", "song_content", "song_first_lyric", "song_date", "song_title_ascii", "song_lastview", "song_isfavorite", "song_rhythm"};
        Cursor c = db.query("SongTbl", cols, null, null, null, null, null);
        int song_id = c.getColumnIndex("song_id");
        int song_title = c.getColumnIndex("song_title");
        int song_link = c.getColumnIndex("song_link");
        int song_content = c.getColumnIndex("song_content");
        int song_first_lyric = c.getColumnIndex("song_first_lyric");
        int song_date = c.getColumnIndex("song_date");
        int song_title_ascii = c.getColumnIndex("song_title_ascii");
        int song_lastview = c.getColumnIndex("song_lastview");
        int song_isfavorite = c.getColumnIndex("song_isfavorite");
        int song_rhythm = c.getColumnIndex("song_rhythm");

        for(c.moveToFirst(); !c.isAfterLast(); c.moveToNext()){
            songs.add(new Song(
                        Integer.parseInt(c.getString(song_id)),
                        c.getString(song_title),
                        c.getString(song_link),
                        c.getString(song_content),
                        c.getString(song_first_lyric),
                        new Date(),
                        c.getString(song_title_ascii),
                        Integer.parseInt(c.getString(song_lastview)),
                        Integer.parseInt(c.getString(song_isfavorite))  ,
                        c.getString(song_rhythm),
                        getArtistBySongId(Integer.parseInt(c.getString(song_id))).get(0)
                    ));
        }
        c.close();
        return songs;
    }

    public ArrayList<Artist> getListArtist(){
        ArrayList<Artist> artists = new ArrayList<Artist>();
        String[] cols = {"artist_id", "artist_name", "artist_ascii"};
        Cursor c = db.query("ArtistTbl", cols, null, null, null, null, null);
        int id = c.getColumnIndex("artist_id");
        int artist_name = c.getColumnIndex("artist_name");
        int artist_ascii = c.getColumnIndex("artist_ascii");
        for(c.moveToFirst(); !c.isAfterLast(); c.moveToNext()){
            artists.add(new Artist(Integer.parseInt(c.getString(id)), c.getString(artist_name), c.getString(artist_ascii), getQuantitySongofArtist(c.getString(artist_name))));
        }
        return artists;
    }

    public int getQuantitySongofArtist(String name){
        int i = 0;
        ArrayList<Song> songs = getListSong();
        for(Song song: songs){
            if(song.getAuthors().equals(name)){
                i++;
            }
        }
        return i;
    }

    private static class OpenHelper extends SQLiteOpenHelper{

        /* Function use to create a OpenHelper */
        public OpenHelper(Context context){
            super(context, DATABASE_NAME, null, DATABASE_VERSION);
        }

        /* Create tables and its columns */
        @Override
        public void onCreate(SQLiteDatabase arg0){
            String sqlArtist =
                    "CREATE TABLE ArtistTbl " +
                            "(_id INTEGER PRIMARY KEY AUTOINCREMENT," +
                            "artist_id INTEGER," +
                            "artist_name TEXT NOT NULL," +
                            "artist_ascii TEXT NOT NULL," +
                            "UNIQUE (artist_id) ON CONFLICT REPLACE)";
            arg0.execSQL(sqlArtist);

            String sqlChord =
                    "CREATE TABLE ChordTbl " +
                            "(_id INTEGER PRIMARY KEY AUTOINCREMENT," +
                            "chord_id INTEGER," +
                            "chord_name TEXT NOT NULL," +
                            "chord_relations TEXT," +
                            "UNIQUE (chord_id) ON CONFLICT REPLACE)";
            arg0.execSQL(sqlChord);

            String sqlSong= "CREATE TABLE SongTbl " +
                    "(_id INTEGER PRIMARY KEY AUTOINCREMENT," +
                    "song_id INTEGER," +
                    "song_title TEXT NOT NULL," +
                    "song_link TEXT," +
                    "song_content TEXT," +
                    "song_first_lyric TEXT," +
                    "song_date TEXT," +
                    "song_title_ascii TEXT," +
                    "song_lastview INTEGER," +
                    "song_isfavorite INTEGER," +
                    "song_rhythm TEXT," +
                    "UNIQUE (song_id) ON CONFLICT REPLACE)";
            arg0.execSQL(sqlSong);

            String sqlPlaylist =
                    "CREATE TABLE Playlist_Tbl " +
                            "(_id INTEGER PRIMARY KEY AUTOINCREMENT," +
                            "playlist_id INTEGER," +
                            "playlist_name TEXT," +
                            "playlist_description TEXT," +
                            "playlist_date TEXT," +
                            "playlist_public INTEGER," +
                            "UNIQUE (playlist_id) ON CONFLICT REPLACE)";
            arg0.execSQL(sqlPlaylist);

            String sqlPlaylistSong =
                    "CREATE TABLE Playlist_Song_Tbl " +
                            "(_id INTEGER PRIMARY KEY AUTOINCREMENT," +
                            "playlist_id INTEGER REFERENCES Playlist_Tbl(playlist_id)," +
                            "song_id INTEGER REFERENCES SongTbl(song_id)," +
                            "UNIQUE (playlist_id,song_id) ON CONFLICT REPLACE)";
            arg0.execSQL(sqlPlaylistSong);

            String sqlSongAuthor=
                    "CREATE TABLE Song_Author_Tbl " +
                            "(_id INTEGER PRIMARY KEY AUTOINCREMENT," +
                            "song_id INTEGER REFERENCES SongTbl(song_id)," +
                            "artist_id INTEGER REFERENCES ArtistTbl(artist_id)," +
                            "UNIQUE (song_id,artist_id) ON CONFLICT REPLACE)";
            arg0.execSQL(sqlSongAuthor);

            String sqlSongChord =
                    "CREATE TABLE Song_Chord_Tbl " +
                            "(_id INTEGER PRIMARY KEY AUTOINCREMENT," +
                            "song_id INTEGER REFERENCES SongTbl(song_id)," +
                            "chord_id INTEGER REFERENCES ChordTbl(chord_id)," +
                            "UNIQUE (song_id,chord_id) ON CONFLICT REPLACE)";
            arg0.execSQL(sqlSongChord);

            String sqlSongSinger =
                    "CREATE TABLE Song_Singer_Tbl " +
                            "(_id INTEGER PRIMARY KEY AUTOINCREMENT," +
                            "song_id INTEGER REFERENCES SongTbl(song_id)," +
                            "artist_id INTEGER REFERENCES ArtistTbl(artist_id)," +
                            "UNIQUE (song_id,artist_id) ON CONFLICT REPLACE)";
            arg0.execSQL(sqlSongSinger);
        }

        @Override
        public void onUpgrade(SQLiteDatabase arg0, int arg1, int arg2){
            arg0.execSQL("DROP TABLE IF EXISTS " + TABLE_SONG);
            arg0.execSQL("DROP TABLE IF EXISTS " + TABLE_ARTIST);
            arg0.execSQL("DROP TABLE IF EXISTS playlist_tbl");
            arg0.execSQL("DROP TABLE IF EXISTS " + TABLE_CHORD);
            arg0.execSQL("DROP TABLE IF EXISTS Playlist_Song_Tbl");
            arg0.execSQL("DROP TABLE IF EXISTS Song_Author_Tbl");
            arg0.execSQL("DROP TABLE IF EXISTS Song_Chord_Tbl");
            arg0.execSQL("DROP TABLE IF EXISTS Song_Singer_Tbl");
            onCreate(arg0);
        }
    }
}

