package pacmanemoticon.uet.gam4u.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

/**
 * Created by Nam on 11/28/2015.
 */
public class SongDatabaseHelper extends SQLiteOpenHelper implements SongDatabase{

    public SongDatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        doCreateTable(db);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        Log.w(SongDatabaseHelper.class.getName(),
                "Upgrading database from version " + oldVersion + " to "
                        + newVersion + ", which will destroy all old data");
        onCreate(db);
    }

    public void doCreateTable(SQLiteDatabase database){
        doCreateArtistTable(database);
        doCreateChordTable(database);
        doCreateSongTable(database);
        doCreatePlayistTable(database);
        doCreatePlayistSongTable(database);
        doCreateSongAuthorTable(database);
        doCreateSongChordTable(database);
        doCreateSongSingerTable(database);
    }


    public void doCreateArtistTable(SQLiteDatabase database)
    {
        String sql="CREATE TABLE ArtistTbl " +
                "(_id INTEGER PRIMARY KEY AUTOINCREMENT," +
                "artist_id INTEGER," +
                "artist_name TEXT NOT NULL," +
                "artist_ascii TEXT NOT NULL," +
                "UNIQUE (artist_id) ON CONFLICT REPLACE)";
        database.execSQL(sql);
    }

    public void doCreateChordTable(SQLiteDatabase database){
        String sql="CREATE TABLE ChordTbl " +
                "(_id INTEGER PRIMARY KEY AUTOINCREMENT," +
                "chord_id INTEGER," +
                "chord_name TEXT NOT NULL," +
                "chord_relations TEXT," +
                "UNIQUE (chord_id) ON CONFLICT REPLACE)";
        database.execSQL(sql);
    }

    public void doCreateSongTable(SQLiteDatabase database){
        String sql= "CREATE TABLE SongTbl " +
                "(_id INTEGER PRIMARY KEY AUTOINCREMENT," +
                "song_id INTEGER,song_title TEXT NOT NULL," +
                "song_link TEXT," +
                "song_content TEXT," +
                "song_first_lyric TEXT," +
                "song_date TEXT," +
                "song_title_ascii TEXT," +
                "song_lastview INTEGER," +
                "song_isfavorite INTEGER," +
                "song_rhythm TEXT," +
                "UNIQUE (song_id) ON CONFLICT REPLACE)";
        database.execSQL(sql);
    }

    public void doCreatePlayistTable(SQLiteDatabase database){
        String sql="CREATE TABLE Playlist_Tbl " +
                "(_id INTEGER PRIMARY KEY AUTOINCREMENT," +
                "playlist_id INTEGER," +
                "playlist_name TEXT," +
                "playlist_description TEXT," +
                "playlist_date TEXT," +
                "playlist_public INTEGER," +
                "UNIQUE (playlist_id) ON CONFLICT REPLACE)";
        database.execSQL(sql);
    }

    public void doCreatePlayistSongTable(SQLiteDatabase database){
        String sql = "CREATE TABLE Playlist_Song_Tbl " +
                "(_id INTEGER PRIMARY KEY AUTOINCREMENT," +
                "playlist_id INTEGER REFERENCES Playlist_Tbl(playlist_id)," +
                "song_id INTEGER REFERENCES SongTbl(song_id)," +
                "UNIQUE (playlist_id,song_id) ON CONFLICT REPLACE)";
        database.execSQL(sql);
    }

    public void doCreateSongAuthorTable(SQLiteDatabase database){
        String sql= "CREATE TABLE Song_Author_Tbl " +
                "(_id INTEGER PRIMARY KEY AUTOINCREMENT," +
                "song_id INTEGER REFERENCES SongTbl(song_id)," +
                "artist_id INTEGER REFERENCES ArtistTbl(artist_id)," +
                "UNIQUE (song_id,artist_id) ON CONFLICT REPLACE)";
        database.execSQL(sql);
    }

    public void doCreateSongChordTable(SQLiteDatabase database){
        String sql = "CREATE TABLE Song_Chord_Tbl " +
                "(_id INTEGER PRIMARY KEY AUTOINCREMENT," +
                "song_id INTEGER REFERENCES SongTbl(song_id)," +
                "chord_id INTEGER REFERENCES ChordTbl(chord_id)," +
                "UNIQUE (song_id,chord_id) ON CONFLICT REPLACE)";
        database.execSQL(sql);
    }

    public void doCreateSongSingerTable(SQLiteDatabase database){
        String sql = "CREATE TABLE Song_Singer_Tbl " +
                "(_id INTEGER PRIMARY KEY AUTOINCREMENT," +
                "song_id INTEGER REFERENCES SongTbl(song_id)," +
                "artist_id INTEGER REFERENCES ArtistTbl(artist_id)," +
                "UNIQUE (song_id,artist_id) ON CONFLICT REPLACE)";
        database.execSQL(sql);
    }

}
