package pacmanemoticon.uet.gam4u.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import pacmanemoticon.uet.gam4u.R;

/**
 * Created by Nam on 11/30/2015.
 */
public class ChordFragment extends Fragment{
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.activity_chord,container,false);
        return v;
    }
}
