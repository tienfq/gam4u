package pacmanemoticon.uet.gam4u.model;

import java.util.Objects;

/**
 * Created by anhdt on 11/29/2015.
 */
public class Artist {
    public int id;
    public int artistId;
    public String artistName;
    public String artistAscii;
    public int quantitySong;

    public Artist(int id, int artistId, String artistName, String artistAscii){
        this.id = id;
        this.artistId = artistId;
        this.artistAscii = artistAscii;
        this.artistName = artistName;
    }

    public Artist(int artistId, String artistName, String artistAscii){
        this.artistAscii = artistAscii;
        this.artistName = artistName;
        this.artistId = artistId;
    }

    public Artist(int artistId, String artistName, String artistAscii, int quantitySong){
        this.artistAscii = artistAscii;
        this.artistName = artistName;
        this.artistId = artistId;
        this.quantitySong = quantitySong;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setArtistId(int artistId) {
        this.artistId = artistId;
    }

    public void setArtistName(String artistName) {
        this.artistName = artistName;
    }

    public void setArtistAscii(String artistAscii) {
        this.artistAscii = artistAscii;
    }

    public void setQuantitySong(int quantitySong) {
        this.quantitySong = quantitySong;
    }

    public int getArtistId() {

        return artistId;
    }

    public int getId() {
        return id;
    }

    public String getArtistName() {
        return artistName;
    }

    public int getQuantitySong() {
        return quantitySong;
    }

    public String getArtistAscii() {
        return artistAscii;
    }

    public String toString(){
        return id + " " + artistId + " " + artistName + " " + artistAscii + "\n";
    }
}
