package pacmanemoticon.uet.gam4u.model;

import com.dqt.libs.chorddroid.components.ChordTextureView;

/**
 * Created by anhdt on 11/29/2015.
 */
public class Chord {
    public int chordId;
    public int id;
    public String name;
    public String relations;
    public ChordTextureView chordView;

    public Chord(int id, int chordId, String name, String relations) {
        id = id;
        chordId = chordId;
        name = name;
        relations = relations;
    }

    public Chord(int chordId, String name, String relations) {
        chordId = chordId;
        name = name;
        relations = relations;
    }
}
