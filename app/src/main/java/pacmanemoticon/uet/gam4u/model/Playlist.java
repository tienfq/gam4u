package pacmanemoticon.uet.gam4u.model;

import android.content.Context;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by anhdt on 11/29/2015.
 */
public class Playlist {
    public Date date;
    public int id;
    public int isPublic;
    public String playlistDescription;
    public int playlistId;
    public String playlistName;
    private List<Integer> songIds;
    private List<Song> songs;

    public Playlist(int id, int playlistId, String playlistName, String playlistDescription, Date date, int isPublic){
        id = id;
        playlistDescription = playlistDescription;
        playlistId = playlistId;
        playlistName = playlistName;
        isPublic = isPublic;
    }

    public Playlist(int playlistId, String playlistName, String playlistDescription, Date date, int isPublic){
        playlistDescription = playlistDescription;
        playlistId = playlistId;
        playlistName = playlistName;
        isPublic = isPublic;
    }

    public Playlist(int playlistId, String playlistName, String playlistDescription, Date date, int isPublic, List<Integer> songIds, List<Song> songs){
        playlistDescription = playlistDescription;
        playlistId = playlistId;
        playlistName = playlistName;
        isPublic = isPublic;
        songIds = songIds;
        songs = songs;
    }
}
