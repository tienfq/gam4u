package pacmanemoticon.uet.gam4u.model;

import java.util.*;


/**
 * Created by anhdt on 11/29/2015.
 */
public class Song {
    private String authors;
    private List<Chord> chords;
    private String content;
    public Date date;
    public String firstLyric;
    public long isFavorite;
    public long lastView;
    public String link;
    public String rhythm;
    private List<Artist> singers;
    public int songId;
    public String title;
    public String titleAscii;
    public int id;

    public Song(){

    }

    public Song(int id, int songId, String title, String link, String content, String firstLyric, Date date, String titleAscii, int lastView, int isFavorite, String rhythm){
        this.id = id;
        this.songId = songId;
        this.link = link;
        this.content = content;
        this.firstLyric = firstLyric;
        this.date = date;
        this.title = title;
        this.titleAscii = titleAscii;
        this.lastView = lastView;
        this.isFavorite = isFavorite;
        this.rhythm = rhythm;
    }

    public Song(int songId, String title, String link, String content, String firstLyric, Date date, String titleAscii, int lastView, int isFavorite, String rhythm){
        this.songId = songId;
        this.link = link;
        this.content = content;
        this.firstLyric = firstLyric;
        this.date = date;
        this.title = title;
        this.titleAscii = titleAscii;
        this.lastView = lastView;
        this.isFavorite = isFavorite;
        this.rhythm = rhythm;
    }

    public Song(int songId, String title, String link, String content, String firstLyric, Date date, String titleAscii, int lastView, int isFavorite, String rhythm, String authors){
        this.id = id;
        this.songId = songId;
        this.link = link;
        this.content = content;
        this.firstLyric = firstLyric;
        this.date = date;
        this.title = title;
        this.titleAscii = titleAscii;
        this.lastView = lastView;
        this.isFavorite = isFavorite;
        this.rhythm = rhythm;
        this.authors = authors;
    }

    public Song(int id, int songId, String title, String link, String content, String firstLyric, Date date, String titleAscii, int lastView, int isFavorite, String rhythm, String authors, List<Chord> chords){
        this.id = id;
        this.songId = songId;
        this.link = link;
        this.content = content;
        this.firstLyric = firstLyric;
        this.date = date;
        this.title = title;
        this.titleAscii = titleAscii;
        this.lastView = lastView;
        this.isFavorite = isFavorite;
        this.rhythm = rhythm;
        this.authors = authors;
        this.chords = chords;
    }
    public String getAuthors() {
        return authors;
    }

    public void setAuthors(String authors) {
        this.authors = authors;
    }

    public List<Chord> getChords() {
        return chords;
    }

    public void setChords(List<Chord> chords) {
        this.chords = chords;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public long getIsFavorite() {
        return isFavorite;
    }

    public void setIsFavorite(long isFavorite) {
        this.isFavorite = isFavorite;
    }

    public List<Artist> getSingers() {
        return singers;
    }

    public void setSingers(List<Artist> singers) {
        this.singers = singers;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getFirstLyric() {
        return firstLyric;
    }

    public void setFirstLyric(String firstLyric) {
        this.firstLyric = firstLyric;
    }
}
